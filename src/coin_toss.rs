use crate::{
    shamir_feldman::coalesce_points,
    transcript::{RandTranscript, RistrettoTranscript},
};
use curve25519_dalek::{
    constants::RISTRETTO_BASEPOINT_TABLE as G, ristretto::RistrettoPoint, scalar::Scalar,
};

pub struct KeyShare {
    i: u64,
    x: Scalar,
}

pub struct VerifKey {
    // FIXME: vector!
    hs: Vec<RistrettoPoint>,
}

pub struct Share {
    i: u64,
    xH: RistrettoPoint,
    c: Scalar,
    z: Scalar,
}

pub trait ThresCoinToss {
    type Transcript;
    type KeyShare;
    type VerifKey;
    type Share;
    type ValidShare;

    fn compute_share(t: Self::Transcript, ks: &Self::KeyShare, m: &[u8]) -> Self::Share;

    fn validate_share(
        t: Self::Transcript,
        vk: &Self::VerifKey,
        s: Self::Share,
        m: &[u8],
    ) -> Result<Self::ValidShare, ()>;

    fn coalesce(t: Self::Transcript, shares: &[Self::ValidShare]) -> (Self::Transcript, u64);
}

pub struct CoinToss<T>(core::marker::PhantomData<*const T>);

impl<T> ThresCoinToss for CoinToss<T>
where
    T: RistrettoTranscript + RandTranscript + Clone,
{
    type Transcript = T;
    type KeyShare = KeyShare;
    type VerifKey = VerifKey;
    type Share = Share;
    type ValidShare = (u64, RistrettoPoint);

    fn compute_share(
        t: Self::Transcript,
        KeyShare { x, i }: &Self::KeyShare,
        m: &[u8],
    ) -> Self::Share {
        let (t, H) = t.append_slice(m).challenge_point();
        let (t, s) = t.witness_scalar(&x).generate_random_scalar();
        let sG = &G * &s;
        let sH = H * &s;
        let xH = H * x;
        let (_, c) = t
            .append_point(&H.compress())
            .append_point(&(&G * &x).compress())
            .append_point(&sG.compress())
            .append_point(&sH.compress())
            .append_point(&xH.compress())
            .challenge_scalar();
        let z = &s + x * &c;

        Share { i: *i, xH, c, z }
    }

    fn validate_share(
        t: Self::Transcript,
        VerifKey { hs }: &Self::VerifKey,
        Share { i, xH, c, z }: Self::Share,
        m: &[u8],
    ) -> Result<Self::ValidShare, ()> {
        let (t, H) = t.append_slice(m).challenge_point();
        let sG = &G * &z - &hs[i as usize - 1] * &c;
        let sH = &H * &z - &xH * &c;
        let (_, c2) = t
            .append_point(&H.compress())
            .append_point(&hs[i as usize - 1].compress())
            .append_point(&sG.compress())
            .append_point(&sH.compress())
            .append_point(&xH.compress())
            .challenge_scalar();

        if c == c2 {
            Ok((i, xH))
        } else {
            Err(())
        }
    }

    fn coalesce(t: Self::Transcript, shares: &[Self::ValidShare]) -> (Self::Transcript, u64) {
        let r = coalesce_points(shares);
        let (t, v) = t.append_point(&r.compress()).challenge_u64();
        (t, v)
    }
}

#[cfg(all(test, feature = "std"))]
mod test {
    use super::*;
    use crate::{shamir_feldman::shamir_feldman_vec, transcript::RandMerlin};

    fn weak_keygen(
        rng: &mut (impl rand::CryptoRng + rand::Rng),
        k: usize,
        n: usize,
    ) -> (VerifKey, Vec<KeyShare>) {
        let secret = Scalar::random(rng);
        let (_, shares) = shamir_feldman_vec(rng, &secret, k, n);
        (
            VerifKey {
                hs: shares.iter().map(|x| &G * &x).collect(),
            },
            (1..).zip(shares).map(|(i, x)| KeyShare { i, x }).collect(),
        )
    }

    #[test]
    fn check() {
        let rng = &mut rand::thread_rng();

        let (vk, shares) = weak_keygen(rng, 3, 9);
        let t = RandMerlin::new(
            merlin::Transcript::new(b"threshold coin tossing sanity checks"),
            rng,
        );

        let m = b"Hello world";
        let shares = [
            CoinToss::validate_share(
                t.clone(),
                &vk,
                CoinToss::compute_share(t.clone(), &shares[3], m),
                m,
            )
            .expect("invalid share 3"),
            CoinToss::validate_share(
                t.clone(),
                &vk,
                CoinToss::compute_share(t.clone(), &shares[1], m),
                m,
            )
            .expect("invalid share 1"),
            CoinToss::validate_share(
                t.clone(),
                &vk,
                CoinToss::compute_share(t.clone(), &shares[7], m),
                m,
            )
            .expect("invalid share 7"),
            CoinToss::validate_share(
                t.clone(),
                &vk,
                CoinToss::compute_share(t.clone(), &shares[8], m),
                m,
            )
            .expect("invalid share 8"),
        ];

        let (_, b1) = CoinToss::coalesce(t.clone(), &shares[..3]);
        let (_, b2) = CoinToss::coalesce(t.clone(), &shares[1..]);
        assert_eq!(b1, b2);
    }
}
