use crate::{
    shamir_feldman::coalesce_points,
    transcript::{RandTranscript, RistrettoTranscript},
};
use curve25519_dalek::{
    constants::RISTRETTO_BASEPOINT_TABLE as G, ristretto::RistrettoPoint, scalar::Scalar,
};

pub struct KeyShare {
    i: u64,
    x: Scalar,
}

pub struct PublicKey {
    kG: RistrettoPoint,
    H: RistrettoPoint,
}

pub struct VerifKey {
    // FIXME: vector!
    hs: Vec<RistrettoPoint>,
}

pub struct Ciphertext {
    c: [u8; 32],
    rG: RistrettoPoint,
    rH: RistrettoPoint,
    e: Scalar,
    f: Scalar,
}

pub struct ValidCt {
    c: [u8; 32],
    rG: RistrettoPoint,
}

pub struct Share {
    i: u64,
    xrG: RistrettoPoint,
    ei: Scalar,
    fi: Scalar,
}

pub trait ThresEnc {
    type Transcript;
    type KeyShare;
    type PublicKey;
    type VerifKey;
    type Message;
    type Ciphertext;
    type ValidCt;
    type Share;
    type ValidShare;

    fn encrypt(
        t: Self::Transcript,
        pk: &Self::PublicKey,
        m: &Self::Message,
    ) -> (Self::Transcript, Self::Ciphertext);

    fn validate_ct(
        t: Self::Transcript,
        ct: Self::Ciphertext,
        pk: &PublicKey,
    ) -> Result<(Self::Transcript, ValidCt), ()>;

    fn decrypt_share(t: Self::Transcript, ks: &Self::KeyShare, vc: &Self::ValidCt) -> Self::Share;

    fn validate_share(
        t: Self::Transcript,
        vk: &Self::VerifKey,
        vc: &Self::ValidCt,
        s: Self::Share,
    ) -> Result<Self::ValidShare, ()>;

    fn decrypt(
        t: Self::Transcript,
        shares: &[Self::ValidShare],
        vc: Self::ValidCt,
    ) -> (Self::Transcript, Self::Message);
}

pub struct TDH2<T>(core::marker::PhantomData<*const T>);

impl<T> ThresEnc for TDH2<T>
where
    T: RistrettoTranscript + RandTranscript + Clone,
{
    type Transcript = T;
    type KeyShare = KeyShare;
    type PublicKey = PublicKey;
    type VerifKey = VerifKey;
    type Message = [u8; 32];
    type Ciphertext = Ciphertext;
    type ValidCt = ValidCt;
    type Share = Share;
    type ValidShare = (u64, RistrettoPoint);

    fn encrypt(
        t: Self::Transcript,
        PublicKey { kG, H }: &Self::PublicKey,
        m: &Self::Message,
    ) -> (T, Self::Ciphertext) {
        let (t, s) = t.generate_random_scalar();
        let (t, r) = t.generate_random_scalar();
        let (t1, t2) = t.fork();

        let rkG = (&r * kG).compress();
        let mut c = [0u8; 32];
        let t = t1.append_point(&rkG).challenge_bytes(&mut c);
        for (a, b) in c.iter_mut().zip(m) {
            *a ^= b;
        }

        let rG = &G * &r;
        let rH = H * &r;
        let sG = &G * &s;
        let sH = H * &s;
        let (_, e) = t2
            .append_bytes(&c)
            .append_point(&rG.compress())
            .append_point(&sG.compress())
            .append_point(&rH.compress())
            .append_point(&sH.compress())
            .challenge_scalar();
        let f = &s + &r * &e;

        (t, Ciphertext { c, rG, rH, e, f })
    }

    fn validate_ct(
        t: Self::Transcript,
        Ciphertext { c, rG, rH, e, f }: Self::Ciphertext,
        PublicKey { H, .. }: &PublicKey,
    ) -> Result<(Self::Transcript, ValidCt), ()> {
        let sG = &G * &f - rG * e;
        let sH = H * &f - rH * e;

        let (t, e2) = t
            .fork()
            .1
            .append_bytes(c)
            .append_point(&rG.compress())
            .append_point(&sG.compress())
            .append_point(&rH.compress())
            .append_point(&sH.compress())
            .challenge_scalar();

        if e == e2 {
            Ok((t, ValidCt { c, rG }))
        } else {
            Err(())
        }
    }

    fn decrypt_share(
        t: Self::Transcript,
        KeyShare { x, i }: &Self::KeyShare,
        ValidCt { rG, .. }: &Self::ValidCt,
    ) -> Self::Share {
        let (t, z) = t.witness_scalar(&x).generate_random_scalar();
        let xrG = x * rG;
        let zrG = &z * rG;
        let zG = &z * &G;

        let (_, ei) = t
            .append_u64(*i)
            .append_point(&xrG.compress())
            .append_point(&zrG.compress())
            .append_point(&zG.compress())
            .challenge_scalar();
        let fi = &z + x * ei;

        Share { i: *i, xrG, ei, fi }
    }

    fn validate_share(
        t: Self::Transcript,
        VerifKey { hs, .. }: &Self::VerifKey,
        ValidCt { rG, .. }: &Self::ValidCt,
        Share { i, xrG, ei, fi }: Self::Share,
    ) -> Result<Self::ValidShare, ()> {
        let zrG = rG * &fi - &xrG * ei;
        let zG = &G * &fi - &hs[i as usize - 1] * &ei;

        let (_, ei2) = t
            .append_u64(i)
            .append_point(&xrG.compress())
            .append_point(&zrG.compress())
            .append_point(&zG.compress())
            .challenge_scalar();

        if ei == ei2 {
            Ok((i as u64, xrG))
        } else {
            Err(())
        }
    }

    fn decrypt(
        t: Self::Transcript,
        shares: &[Self::ValidShare],
        ValidCt { c, .. }: Self::ValidCt,
    ) -> (T, Self::Message) {
        let mut m = [0u8; 32];
        let t = t
            .fork()
            .0
            .append_point(&coalesce_points(shares).compress())
            .challenge_bytes(&mut m);

        for (a, b) in m.iter_mut().zip(&c) {
            *a ^= b;
        }

        (t, m)
    }
}

#[cfg(all(test, feature = "std"))]
mod test {
    use super::*;
    use crate::{
        shamir_feldman::shamir_feldman_vec,
        transcript::{RandMerlin, Transcript},
    };

    fn weak_keygen(
        rng: &mut (impl rand::CryptoRng + rand::Rng),
        k: usize,
        n: usize,
    ) -> (PublicKey, VerifKey, Vec<KeyShare>) {
        let secret = Scalar::random(rng);
        let (commits, shares) = shamir_feldman_vec(rng, &secret, k, n);
        (
            PublicKey {
                kG: commits[0],
                H: RistrettoPoint::random(rng),
            },
            VerifKey {
                hs: shares.iter().map(|x| &G * &x).collect(),
            },
            (1..).zip(shares).map(|(i, x)| KeyShare { i, x }).collect(),
        )
    }

    #[test]
    fn check() {
        let rng = &mut rand::thread_rng();
        let (pk, vk, shares) = weak_keygen(rng, 3, 9);
        let t1 = RandMerlin::new(merlin::Transcript::new(b"TDH2 sanity check"), rng);
        let t2 = RandMerlin::new(merlin::Transcript::new(b"TDH2 sanity check"), rng);

        // Transcript-bound TDH2; threshold decryption
        let (t1, ct) = TDH2::encrypt(t1, &pk, b"0123456789abcdef0123456789abcdef");
        let (t3, vc) = TDH2::validate_ct(t2.clone(), ct, &pk).expect("invalid header");

        let (t3_1a, t3_3a, t3_7a) = (t3.clone(), t3.clone(), t3.clone());
        let (t3_1b, t3_3b, t3_7b) = (t3.clone(), t3.clone(), t3.clone());
        let shares = &[
            TDH2::validate_share(t3_3b, &vk, &vc, TDH2::decrypt_share(t3_3a, &shares[3], &vc))
                .expect("invalid key share"),
            TDH2::validate_share(t3_1b, &vk, &vc, TDH2::decrypt_share(t3_1a, &shares[1], &vc))
                .expect("invalid key share"),
            TDH2::validate_share(t3_7b, &vk, &vc, TDH2::decrypt_share(t3_7a, &shares[7], &vc))
                .expect("invalid key share"),
        ];
        let (t2, m) = TDH2::decrypt(t2, shares, vc);

        // the recoverd message is correct
        assert_eq!(&m, b"0123456789abcdef0123456789abcdef");

        // the two transcripts are in equivalent states
        let mut t1_kdf_out = [0u8; 16];
        let mut t2_kdf_out = [0u8; 16];
        t1.challenge_bytes(&mut t1_kdf_out);
        t2.challenge_bytes(&mut t2_kdf_out);
        assert_eq!(t1_kdf_out, t2_kdf_out);
    }
}
