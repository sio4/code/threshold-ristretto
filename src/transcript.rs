use curve25519_dalek::{
    ristretto::{CompressedRistretto, RistrettoPoint},
    scalar::Scalar,
};
use rand_core::{CryptoRng, RngCore};

// session-type based transcript hashing. Forking a transcript is only transparent

/// A low level transcript core backend. See `RistrettoTranscript` for transcripts consuming
/// and producing scalars and points over the Ristretto group.
pub trait Transcript: Sized {
    /// Append a fixed-width array of bytes to the transcript. Use `append_slice` for variable
    /// length inputs.
    fn append_bytes(self, message: impl AsRef<[u8]>) -> Self;

    /// Append one u64 into the transcript, in little endian order.
    fn append_u64(self, n: u64) -> Self {
        self.append_bytes(n.to_le_bytes())
    }

    /// Append a variable length bytestring. Use `append_bytes` for fixed-size inputs.
    fn append_slice(self, buf: impl AsRef<[u8]>) -> Self {
        let s = buf.as_ref();
        self.append_u64(s.len() as u64).append_bytes(s)
    }

    fn challenge_bytes(self, buf: impl AsMut<[u8]>) -> Self;

    fn challenge_u64(self) -> (Self, u64) {
        let mut buf = [0u8; 8];
        (self.challenge_bytes(&mut buf), u64::from_le_bytes(buf))
    }

    fn challenge_slice(self, mut buf: impl AsMut<[u8]>) -> Self {
        let s = buf.as_mut();
        self.append_u64(s.len() as u64).challenge_bytes(s)
    }

    fn fork(self) -> (Self, Self);
}

/// Witnesses and challenges along the randomized transcript do not contribute to the primary
/// transcript. All contributes to the primary transcript must influence the witness.
pub trait RandTranscript: Transcript {
    type Rng: RngCore + CryptoRng;

    fn with_rng(&mut self) -> &mut Self::Rng;
    fn witness_bytes(self, buf: impl AsRef<[u8]>) -> Self;
    fn generate_random_bytes(mut self, mut buf: impl AsMut<[u8]>) -> Self {
        self.with_rng().fill_bytes(buf.as_mut());
        self
    }
}

pub trait RistrettoTranscript: Transcript {
    fn append_point(self, p: &CompressedRistretto) -> Self {
        self.append_bytes(p.as_bytes())
    }

    fn append_scalar(self, s: &Scalar) -> Self {
        self.append_bytes(s.as_bytes())
    }

    fn challenge_point(mut self) -> (Self, RistrettoPoint) {
        let mut buf = [0u8; 64];
        self = self.challenge_bytes(&mut buf[..]);
        (self, RistrettoPoint::from_uniform_bytes(&buf))
    }

    fn challenge_scalar(mut self) -> (Self, Scalar) {
        let mut buf = [0u8; 64];
        self = self.challenge_bytes(&mut buf[..]);
        (self, Scalar::from_bytes_mod_order_wide(&buf))
    }

    fn witness_point(self, p: &CompressedRistretto) -> Self
    where
        Self: RandTranscript,
    {
        self.witness_bytes(p.as_bytes())
    }

    fn witness_scalar(self, s: &Scalar) -> Self
    where
        Self: RandTranscript,
    {
        self.witness_bytes(s.as_bytes())
    }

    fn generate_random_point(mut self) -> (Self, RistrettoPoint)
    where
        Self: RandTranscript,
    {
        let p = RistrettoPoint::random(self.with_rng());
        (self, p)
    }

    fn generate_random_scalar(mut self) -> (Self, Scalar)
    where
        Self: RandTranscript,
    {
        let s = Scalar::random(self.with_rng());
        (self, s)
    }
}

impl<T> RistrettoTranscript for T where T: Transcript {}

#[cfg(any(test, feature = "merlin"))]
pub use self::merlin_impl::RandMerlin;

#[cfg(any(test, feature = "merlin"))]
mod merlin_impl {
    use super::*;

    impl Transcript for merlin::Transcript {
        fn append_bytes(mut self, message: impl AsRef<[u8]>) -> Self {
            self.append_message(b"linear append", message.as_ref());
            self
        }

        fn challenge_bytes(mut self, mut message: impl AsMut<[u8]>) -> Self {
            merlin::Transcript::challenge_bytes(&mut self, b"linear extract", message.as_mut());
            self
        }

        fn fork(self) -> (Self, Self) {
            (self.clone().append_u64(0), self.append_u64(1))
        }
    }

    #[derive(Clone)]
    pub struct RandMerlin {
        inner: merlin::Transcript,
        witness: merlin::Transcript,
    }

    impl RandMerlin {
        pub fn new(inner: merlin::Transcript, rng: &mut (impl CryptoRng + RngCore)) -> Self {
            let mut witness = inner.clone();
            let mut buf = [0u8; 32];
            rng.fill_bytes(&mut buf);
            witness.append_message(b"fresh rng input", &buf);
            RandMerlin { inner, witness }
        }
    }

    impl Transcript for RandMerlin {
        fn append_bytes(mut self, buf: impl AsRef<[u8]>) -> Self {
            self.inner = self.inner.append_bytes(buf.as_ref());
            self.witness = self.witness.append_bytes(buf);
            self
        }

        fn challenge_bytes(mut self, buf: impl AsMut<[u8]>) -> Self {
            self.inner = self.inner.challenge_bytes(buf);
            self
        }

        fn fork(self) -> (Self, Self) {
            (self.clone().append_u64(0), self.append_u64(1))
        }
    }

    impl RandTranscript for RandMerlin {
        type Rng = Self;

        fn with_rng(&mut self) -> &mut Self::Rng {
            self
        }

        fn witness_bytes(mut self, buf: impl AsRef<[u8]>) -> Self {
            self.witness.append_message(b"witness", buf.as_ref());
            self
        }
    }

    impl CryptoRng for RandMerlin {}

    impl RngCore for RandMerlin {
        fn next_u32(&mut self) -> u32 {
            rand_core::impls::next_u32_via_fill(self)
        }

        fn next_u64(&mut self) -> u64 {
            rand_core::impls::next_u64_via_fill(self)
        }

        fn fill_bytes(&mut self, dest: &mut [u8]) {
            merlin::Transcript::challenge_bytes(&mut self.witness, b"rng fill bytes", dest);
        }

        fn try_fill_bytes(&mut self, dest: &mut [u8]) -> Result<(), rand_core::Error> {
            self.fill_bytes(dest);
            Ok(())
        }
    }
}
