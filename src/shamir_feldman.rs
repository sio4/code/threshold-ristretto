use curve25519_dalek::{
    constants::RISTRETTO_BASEPOINT_TABLE as G, ristretto::RistrettoPoint, scalar::Scalar,
};
use rand_core::{CryptoRng, RngCore};

fn shamir_feldman_inner(
    rng: &mut (impl CryptoRng + RngCore),
    secret: &Scalar,
    coeffs: &mut [Scalar],
    commits: &mut [RistrettoPoint],
    shares: &mut [Scalar],
) {
    assert_eq!(
        commits.len(),
        shares.len() + 1,
        "Commits must be 1 greater than the shares."
    );
    assert!(
        coeffs.len() <= shares.len(),
        "Shares must be greater than the coefficients."
    );

    commits[0] = secret * &G;

    for (coeff, commit) in coeffs.iter_mut().zip(&mut commits[1..]) {
        *coeff = Scalar::random(rng);
        *commit = &*coeff * &G;
    }

    for (x, i) in shares.iter_mut().zip(1u64..) {
        let si = Scalar::from(i);
        let mut z = si;
        *x = *secret;

        for c in &*coeffs {
            *x += c * z;
            z *= si;
        }
    }
}

/// Coalesce K (or more) scalars
pub fn coalesce_scalars(shares: &[(u64, Scalar)]) -> Scalar {
    let mut r = Scalar::zero();

    let js = shares
        .iter()
        .map(|(j, _)| Scalar::from(*j))
        .product::<Scalar>();
    for (i, x) in shares {
        let mut d = Scalar::from(*i);
        for (j, _) in shares {
            if j != i {
                d *= Scalar::from(*j) - Scalar::from(*i);
            }
        }
        // FIXME: batch scalar inverses
        r += &js * d.invert() * x;
    }

    r
}

/// Coalesce K (or more) points
pub fn coalesce_points(shares: &[(u64, RistrettoPoint)]) -> RistrettoPoint {
    let mut r = &G * &Scalar::zero();

    let js = shares
        .iter()
        .map(|(j, _)| Scalar::from(*j))
        .product::<Scalar>();
    for (i, x) in shares {
        let mut d = Scalar::from(*i);
        for (j, _) in shares {
            if j != i {
                d *= Scalar::from(*j) - Scalar::from(*i);
            }
        }
        // FIXME: batch scalar inverses
        r += &js * d.invert() * x;
    }

    r
}

/// Verify an individual share using Feldman's VSS
pub fn feldman_verify(commits: &[RistrettoPoint], i: u64, x: Scalar) -> bool {
    let mut v = commits[0];
    let si = Scalar::from(i);
    let mut z = si;
    for c in &commits[1..] {
        v += c * z;
        z *= si;
    }
    v.compress() == (&x * &G).compress()
}

// FIXME: once we have const-generics; we will probably still need vectors for runtime configured
// thresholds. it might be worth having a reusable context which owns the vectors.
#[cfg(feature = "std")]
pub fn shamir_feldman_vec(
    rng: &mut (impl CryptoRng + RngCore),
    secret: &Scalar,
    k: usize,
    n: usize,
) -> (Vec<RistrettoPoint>, Vec<Scalar>) {
    let mut coeffs = vec![Default::default(); k - 1];
    let mut commits = vec![Default::default(); n + 1];
    let mut shares = vec![Default::default(); n];

    shamir_feldman_inner(rng, secret, &mut *coeffs, &mut *commits, &mut *shares);

    (commits, shares)
}

#[cfg(all(test, feature = "std"))]
mod test {
    use curve25519_dalek::scalar::Scalar;
    use rand::thread_rng;

    use super::{coalesce_scalars, feldman_verify, shamir_feldman_vec};

    #[test]
    fn poc() {
        let rng = &mut thread_rng();
        let secret = Scalar::random(rng);
        let (commits, shares) = shamir_feldman_vec(rng, &secret, 3, 9);

        // any combination of k (or more) shares recovers the secret
        assert_eq!(
            secret,
            coalesce_scalars(
                &*(1u64..)
                    .zip(shares.clone())
                    .take(3)
                    .collect::<Vec<_>>()
            )
        );

        // verify commits against shares
        for (i, share) in (1..).zip(shares) {
            assert!(feldman_verify(&commits, i, share))
        }
    }
}
