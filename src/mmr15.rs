use crate::{
    shamir_feldman::coalesce_points,
    transcript::{RandTranscript, RistrettoTranscript},
    coin_toss::CoinToss,
};
use curve25519_dalek::{
    constants::RISTRETTO_BASEPOINT_TABLE as G, ristretto::RistrettoPoint, scalar::Scalar,
};
