#![cfg_attr(not(feature = "std"), no_std)]
#![forbid(unsafe_code, rust_2018_idioms)]
#![allow(non_snake_case, clippy::many_single_char_names, clippy::op_ref)]

//! The threshold primitives necessary to implement BEAT and ADKG.
//!
//! BEAT: https://www.cs.unc.edu/~reiter/papers/2018/CCS.pdf
//! ADKG: https://eprint.iacr.org/2019/1015
//!
//! ADKG uses and needs MMR15. BEAT uses MMR14, but may use MMR15 and with minor tweaks CKS00;
//! by using the coin toss as a threshold signature scheme with O(k) signatures. Open question: can
//! the shares be coalesced before validating, such that the result can be validated with a single
//! public key?

/// Session-type oriented transcript hashing
pub mod transcript;

/// Shamir Secret Sharing Scheme / Feldman's Verifiable Secret Sharing
pub mod shamir_feldman;

/// CCA-secure labeled threshold encryption.
///
/// TDH1 assumes: CDH
/// TDH2 assumes: DDH
///
/// 'Securing Threshold Cryptosystems against Chosen Ciphertext Attack'
/// - Victor Shoup and Rosario Gennaro. September 18, 2001.
///
/// https://www.shoup.net/papers/thresh1.pdf
pub mod tdh2;

/// Diffie-Hellman Based Threshold Coin-Tossing Scheme.
///
/// Assumes: CDH in the ROM if k = t + 1, DDH otherwise
///
/// Random Oracles in Constantinople: Practical Asynchronous Byzantine Agreement using Cryptography
/// - Christian Cachin, Klaus Kursawe, Victor Shoup. August 14, 2000
///
/// https://eprint.iacr.org/2000/034.pdf
pub mod coin_toss;

/// Signature-Free Asynchronous Binary Byzantine Consensus with `t < n/3`, O(n2) Messages, and O(1)
/// Expected Time
/// - Achour Most́efaoui, Hamouma Moumen, Michel Raynal. 2015
///
/// https://hal.archives-ouvertes.fr/hal-01176110/file/JACM.pdf
pub mod mmr15;

// Provably Secure Distributed Schnorr Signatures and a (t, n) Threshold Scheme for Implicit
// Certificates
// - Douglas R. Stinson, Reto Strobl, 2001
//
// Assumes: CDH or DDH (?)
//
// http://cacr.uwaterloo.ca/techreports/2001/corr2001-13.ps
// FIXME: needs ADKG for each signature.
//
// Go impl exists for ethereum.
// https://github.com/smartcontractkit/chainlink/blob/develop/core/services/signatures/ethdss/ethdss.go
//pub mod schnorr;
